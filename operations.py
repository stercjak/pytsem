import numpy as np
from PIL import Image


def change_brightness(pic, factor):
    data = np.asarray(pic, dtype=np.float)
    R, G, B = data[:, :, 0], data[:, :, 1], data[:, :, 2]
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            cutR = R[i, j]
            cutG = G[i, j]
            cutB = B[i, j]
            data[i][j] = [
                min(max(int(cutR * factor), 0), 255),
                min(max(int(cutG * factor), 0), 255),
                min(max(int(cutB * factor), 0), 255)
            ]
    return Image.fromarray(np.asarray(data, dtype=np.uint8), "RGB")


def invert(pic):
    data = np.asarray(pic, dtype=np.float)
    R, G, B = data[:, :, 0], data[:, :, 1], data[:, :, 2]
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            cutR = R[i, j]
            cutG = G[i, j]
            cutB = B[i, j]
            data[i][j] = [
                255 - cutR,
                255 - cutG,
                255 - cutB
            ]
    return Image.fromarray(np.asarray(data, dtype=np.uint8), "RGB")


def shades_of_grey(pic):
    data = np.asarray(pic, dtype=np.float)
    R, G, B = data[:, :, 0], data[:, :, 1], data[:, :, 2]
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            cutR = R[i, j]
            cutG = G[i, j]
            cutB = B[i, j]
            shade_of_grey = cutR * 0.299 + cutG * 0.587 + cutB * 0.114
            data[i][j] = [
                shade_of_grey,
                shade_of_grey,
                shade_of_grey
            ]
    return Image.fromarray(np.asarray(data, dtype=np.uint8), "RGB")


def highlight_edges(pic):
    data = np.asarray(pic, dtype=np.float)
    mask = np.array([
            [-1, -1, -1],
            [-1, 8, -1],
            [-1, -1, -1]
        ])
    res = [[[0, 0, 0] for i in range(data.shape[1])] for j in range(data.shape[0])]
    R, G, B = data[:, :, 0], data[:, :, 1], data[:, :, 2]
    for i in range(data.shape[0] - 2):
        for j in range(data.shape[1] - 2):
            cutR = R[i:i + 3, j:j + 3]
            cutG = G[i:i + 3, j:j + 3]
            cutB = B[i:i + 3, j:j + 3]
            res[i][j] = [
                min(max(int((cutR * mask).sum()), 0), 255),
                min(max(int((cutG * mask).sum()), 0), 255),
                min(max(int((cutB * mask).sum()), 0), 255)
            ]
    return Image.fromarray(np.asarray(res, dtype=np.uint8), "RGB")


def mirror(pic, horizontal, vertical):
    data = np.asarray(pic, dtype=np.float)
    res = [[[0, 0, 0] for i in range(data.shape[1])] for j in range(data.shape[0])]
    R, G, B = data[:, :, 0], data[:, :, 1], data[:, :, 2]
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            cutR = R[data.shape[0] - 1 - i if vertical else i, data.shape[1] - 1 - j if horizontal else j]
            cutG = G[data.shape[0] - 1 - i if vertical else i, data.shape[1] - 1 - j if horizontal else j]
            cutB = B[data.shape[0] - 1 - i if vertical else i, data.shape[1] - 1 - j if horizontal else j]
            res[i][j] = [cutR, cutG, cutB]
    return Image.fromarray(np.asarray(res, dtype=np.uint8), "RGB")


def rotate(pic, left, right):
    pic = mirror(pic, horizontal=left, vertical=right)
    data = np.asarray(pic, dtype=np.float)
    return Image.fromarray(np.asarray(np.transpose(data, axes=[1, 0, 2]), dtype=np.uint8), "RGB")
