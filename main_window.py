import os
from PIL.ImageQt import ImageQt
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QMainWindow, QFileDialog, QLabel, QMessageBox, QSizePolicy
from PIL import Image
import operations
from qt.mainwindow import Ui_MainWindow


class MainWindow(QMainWindow, Ui_MainWindow):
    TEMP_PATH = "temp/tmp.jpg"

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setupUi(self)

        self.label = QLabel()
        self.verticalLayout.addWidget(self.label)
        self.label.show()
        self.label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.pic = None
        self.menuBar.addAction("Nahrát obrázek", self.loadPicture)

    def setupToolbar(self):
        """
        setup toolbar
        """
        self.menuBar.addAction("Uložit", self.savePicture)
        operations_menu = self.menuBar.addMenu("Operace")
        operations_menu.addAction("Zesvětlit", self.brighten)
        operations_menu.addAction("Ztmavit", self.darken)
        operations_menu.addAction("Inverze", self.invert)
        operations_menu.addAction("Odstíny šedi", self.grey)
        operations_menu.addAction("Zvýraznit hrany", self.edges)
        operations_menu.addAction("Zrcadlo (horiz)", lambda: self.mirror(horizontal=True))
        operations_menu.addAction("Zrcadlo (vert)", lambda: self.mirror(vertical=True))
        self.menuBar.addAction("Otočit doleva", lambda: self.rotate(left=True))
        self.menuBar.addAction("Otočit doprava", lambda: self.rotate(right=True))

    def displayPicture(self):
        """
        display picture saved in self.pic
        """
        pix = QPixmap()
        ImageQt(self.pic).save(self.TEMP_PATH)
        pix.load(self.TEMP_PATH)
        os.remove(self.TEMP_PATH)
        self.label.setPixmap(pix)
        self.centralWidget().adjustSize()
        self.adjustSize()

    def loadPicture(self):
        """
        load and display picture
        """
        file, options = QFileDialog.getOpenFileName(self, "Obrázek")
        if file == "":
            return
        try:
            pic = Image.open(file)
        except OSError:
            msg = QMessageBox(self)
            msg.setWindowTitle("Chyba")
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Zvolený soubor se nepodařilo načíst.")
            msg.exec_()
            return
        if self.pic is None:
            self.setupToolbar()
        self.pic = pic
        self.displayPicture()
        self.resize(self.label.minimumWidth(), self.label.minimumHeight())

    def savePicture(self):
        file, options = QFileDialog.getSaveFileName(self, "Uložit jako:", filter=".jpg;;.png")
        if file == "":
            return
        ImageQt(self.pic).save(file + options)
        msg = QMessageBox(self)
        msg.setWindowTitle("Uloženo")
        msg.setIcon(QMessageBox.Information)
        msg.setText("Obrázek byl uložen.")
        msg.exec_()

    def brighten(self):
        self.pic = operations.change_brightness(self.pic, 1.25 / 1)
        self.displayPicture()

    def darken(self):
        self.pic = operations.change_brightness(self.pic, 1 / 1.25)
        self.displayPicture()

    def invert(self):
        self.pic = operations.invert(self.pic)
        self.displayPicture()

    def grey(self):
        self.pic = operations.shades_of_grey(self.pic)
        self.displayPicture()

    def edges(self):
        self.pic = operations.highlight_edges(self.pic)
        self.displayPicture()

    def mirror(self, horizontal=False, vertical=False):
        self.pic = operations.mirror(self.pic, horizontal, vertical)
        self.displayPicture()

    def rotate(self, left=False, right=False):
        self.pic = operations.rotate(self.pic, left, right)
        self.displayPicture()
